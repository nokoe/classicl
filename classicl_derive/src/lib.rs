use proc_macro::TokenStream;
use quote::quote;

#[proc_macro_derive(FixedSize)]
pub fn fixed_size_derive(input: TokenStream) -> TokenStream{
    let ast: syn::DeriveInput = syn::parse(input).unwrap();

    let name = &ast.ident;

    let fields = if let syn::Data::Struct(syn::DataStruct {
        fields: syn::Fields::Named(ref fields),
        ..
    }) = ast.data
    {
        fields
    } else {
        panic!("only packets as structs supported");
    };

    let mut size: usize = 0;

    for i in fields.named.iter() {
        if let syn::Type::Path(p) = &i.ty {
            let t = p.path.segments.last().unwrap().ident.to_string();

            // This implementation is kind of dirty but a little better than the old one i guess
            match &t[..] {
                "Vec" => size += 1024,
                "i8" => size += 1,
                "u8" => size += 1,
                "i16" => size += 2,
                "String" => size += 64,
                _=> {
                    let mut buf = String::new();
                    p.path.segments.iter().for_each(|x| buf.push_str(&x.ident.to_string()));
                    panic!("{buf} is not supported");
                }
            }
        }
    }

    let gen = quote! {
        impl FixedSize for #name {
            const SIZE: usize = #size;
        }
    };
    gen.into()
}