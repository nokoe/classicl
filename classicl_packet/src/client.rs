use classicl_serde::FixedSize;
use serde::{Serialize, Deserialize};
use crate::Packet;

#[derive(Default, Debug, FixedSize, Serialize, Deserialize)]
pub struct PlayerIdentification {
    pub protocol_version: u8,
    pub username: String,
    pub verification_key: String,
    pub unused: u8,
}

impl Packet for PlayerIdentification {
    const ID: u8 = 0x00;
}

#[derive(Default, Debug, FixedSize, Serialize, Deserialize)]
pub struct SetBlock {
    pub x: i16,
    pub y: i16,
    pub z: i16,
    pub mode: u8,
    pub block_type: u8,
}

impl Packet for SetBlock {
    const ID: u8 = 0x05;
}

#[derive(Default, Debug, FixedSize, Serialize, Deserialize)]
pub struct PositionOrientation {
    pub player_id: u8,
    pub x: i16,
    pub y: i16,
    pub z: i16,
    pub yaw: u8,
    pub pitch: u8,
}

impl Packet for PositionOrientation {
    const ID: u8 = 0x08;
}

#[derive(Default, Debug, FixedSize, Serialize, Deserialize)]
pub struct Message {
    pub unused: u8,
    pub message: String,
}

impl Packet for Message {
    const ID: u8 = 0x0d;
}