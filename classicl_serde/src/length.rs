use serde::Serialize;

pub trait FixedSize: Serialize + Default {
    fn size() -> usize {
        Self::SIZE
    }

    const SIZE: usize;
}