use ser::Serializer;
use de::Deserializer;
pub use length::FixedSize;
pub use classicl_derive::FixedSize;
use serde::{Serialize, Deserialize};
use error::Result;

mod error;

mod ser;
mod de;
mod length;

pub fn to_bytes<T>(value: T) -> Result<Vec<u8>>
where
    T: Serialize,    
{
    let mut serializer = Serializer {
        output: vec![],
    };

    value.serialize(&mut serializer)?;
    Ok(serializer.output)
}

pub fn from_bytes<'a, T>(b: &'a [u8]) -> Result<T>
where
    T: Deserialize<'a>,
{
    let mut deserializer = Deserializer::from_bytes(b);
    let t = T::deserialize(&mut deserializer)?;
    if deserializer.input.is_empty() {
        Ok(t)
    } else {
        Err(error::Error::WrongPacket)
    }
}